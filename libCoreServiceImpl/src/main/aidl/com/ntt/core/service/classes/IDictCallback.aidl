// IDictCallback.aidl
package com.ntt.core.service.classes;

// Declare any non-default types here with import statements
import com.ntt.core.service.entities.SDictTransResultEntity;

interface IDictCallback {
    void onDictTrans(in SDictTransResultEntity result);
}