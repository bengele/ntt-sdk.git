// ICoreService.aidl
package com.ntt.core.service;

import com.ntt.core.service.entities.SAuthEntity;
import com.ntt.core.service.entities.SDictTransResultEntity;
import com.ntt.core.service.classes.IAuthCallback;
import com.ntt.core.service.classes.IBabyInfoCallback;
import com.ntt.core.service.entities.SBabyInfoEntity;
import com.ntt.core.service.classes.IDictCallback;

interface ICoreService {
    // 查字典
    void dictTrans(int dictType, String query, int timeOut, boolean obtainChinese, in IDictCallback callback);

    //获取版本号
    String getVersion();

    //获取是否测试模式下的对应的域名
    String getDomainHost();

    //设置测试模式
    void setDomainDebug(boolean v);

    //获取域名是否测试模式
    boolean getDomainIsDebugMode();

    //同步获取token
    String getSyncAuthToken(boolean force);

    //获取登录标识
    void getAuthToken(boolean force,in IAuthCallback callback);

    //获取宝宝信息
    void getBabyInfo(in IBabyInfoCallback callback );

    //获取设备ID
    String getDeviceId();

    //MQTT订阅
    void subscribe(String topic,int qos);

    //MQTT取消订阅
    void unsubscribe(String topic);

    //发送信息
    void sendMessage(String topic, String data, int qos, boolean retain);

    // 使能/关闭音效
    void setSoundEffectEnable(int sessionId, boolean enable);
    // 释放音效
    void releaseSoundEffect(int sessionId);
}
