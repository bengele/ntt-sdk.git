package com.ntt.core.service.supports.jumper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.blankj.utilcode.util.GsonUtils;
import com.ntt.core.service.entities.SJumpEntity;
import com.ntt.core.service.entities.player.SConfigEntity;
import com.ntt.core.service.entities.player.SMediaEntity;
import com.ntt.core.service.entities.player.SPlayEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JumperManager {
    private static final String TAG = JumperManager.class.getSimpleName();

    private Context mContext;

    public JumperManager(Context context) {
        mContext = context;
    }


    /**
     * 跳转
     *
     * @param entity
     */
    public boolean doJump(SJumpEntity entity) {
        return doJump(entity, -1);
    }

    /**
     * 跳转
     *
     * @param entity
     */
    public boolean doJump(SJumpEntity entity, int requestCode) {
        //生成跳转Intent
        Intent intent = genJumpIntent(entity);
        String code = entity.getCode();
        String pN = entity.getPackageName();
        String packageName = mContext.getPackageName();
        if (!isImplicitStartActivity(intent)) {
            //传进来的包名为空，或者相等，就用当前的包名
            if (pN == null || packageName.equals(pN)) {
                intent.setClassName(mContext, code);
            } else {
                intent.setClassName(pN, code);
            }
        }
        try {
            if (requestCode != -1 && mContext instanceof Activity) {
                ((Activity) mContext).startActivityForResult(intent, requestCode);
            } else {
                mContext.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 生成Intent
     *
     * @param entity
     * @return
     */
    public Intent genJumpIntent(SJumpEntity entity) {
        Intent intent = new Intent();
        String code = entity.getCode();
        Map<String, Object> args = entity.getParams() == null ? new HashMap<>() : entity.getParams();
        switch (code) {
            case JumpConstants.NATIVE_ALBUM_GROUP_COURSE_DETAIL:
                entity.setCode(JumpConstants.NEW_ALBUM_PUNCH_DETAIL_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                if (args.containsKey("uid")) {
                    long uid = UtMapHelper.getMapObject2Long(args, "uid");
                    intent.putExtra("uid", String.valueOf(uid));
                }
                break;
            case JumpConstants.NATIVE_ALBUM_DETAIL_CODE:
                entity.setCode(JumpConstants.NEW_ALBUM_DETAIL_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                if (args.containsKey("uid")) {
                    long uid = UtMapHelper.getMapObject2Long(args, "uid");
                    intent.putExtra("albumId", uid);
                }
                break;
            case JumpConstants.NATIVE_ALBUM_VIDEO_DETAIL_CODE:
                entity.setCode(JumpConstants.NEW_ALBUM_VIDEO_DETAIL);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_VIDEO);
                if (args.containsKey("uid")) {
                    long uid = UtMapHelper.getMapObject2Long(args, "uid");
                    intent.putExtra("albumId", uid);
                }
                break;
            case JumpConstants.NATIVE_GUIDE_VIDEO_CODE:
                entity.setCode(JumpConstants.NEW_Guild_VIDEO);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_GUIDE);
                if (args.containsKey("index")) {//页面
                    intent.putExtra("index", UtMapHelper.getMapObject2Int(args, "index"));
                }
                if (args.containsKey("position")) {//第几个视频
                    intent.putExtra("position", UtMapHelper.getMapObject2Int(args, "position"));
                }
                break;
            case JumpConstants.NEW_ALBUM_DOWNLOAD_TRACK:
                entity.setCode(JumpConstants.NEW_ALBUM_DOWNLOAD_TRACK);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                if (args.containsKey("uid")) {
                    long uid = UtMapHelper.getMapObject2Long(args, "uid");
                    intent.putExtra("albumId", uid);
                }
                break;
            case JumpConstants.ALBUM_COMBINE_DETAIL:
                entity.setCode(JumpConstants.NEW_ALBUM_SERIES_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                if (args.containsKey("uid")) {
                    long uid = UtMapHelper.getMapObject2Long(args, "uid");
                    intent.putExtra("albumId", uid);
                }
                break;
            case JumpConstants.EDIFY_COURSE_DETAIL_CODE: {
                entity.setCode(JumpConstants.NEW_EDIFY_PACKAGE_DETAIL_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                intent.putExtra("uid", UtMapHelper.getMapObject2Long(args, "uid"));
                break;
            }
            case JumpConstants.NATIVE_EDIFY_PLAN_DETAIL_CODE:
            case JumpConstants.NATIVE_EDIFY_ALBUM_DETAIL: {
                entity.setCode(JumpConstants.NEW_EDIFY_ALBUM_DETAIL_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                long uid = UtMapHelper.getMapObject2Long(args, "uid");
                intent.putExtra("edifyId", uid);

                //是否进入进度调整详情页，
                if (args.containsKey("isProgressAdjust")) {
                    boolean isProgressAdjust = UtMapHelper.getMapObjectBoolean(args, "isProgressAdjust");
                    intent.putExtra("isProgressAdjust", isProgressAdjust);
                }
                break;
            }
            case JumpConstants.NATIVE_BROADCASTER_DETAIL_CODE: {
                entity.setCode(JumpConstants.NEW_ANNOUNCER_DETAIL_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                long uid = UtMapHelper.getMapObject2Long(args, "uid");
                intent.putExtra("uid", uid);
                intent.putExtra("isChannel", UtMapHelper.getMapObjectBoolean(args, "isChannel"));
                break;
            }
            case JumpConstants.NATIVE_ANNOUNCER_HOME:
                entity.setCode(JumpConstants.NEW_ANNOUNCER_HOME_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                break;
            case JumpConstants.NATIVE_ALBUM_CAT_HOME:
                entity.setCode(JumpConstants.NEW_ALBUM_CATEGORY_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                break;
            case JumpConstants.NATIVE_EDIFY_HOME:
                entity.setCode(JumpConstants.NEW_EDIFY_LIST_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                if (args.containsKey("isAddSchedule")) {
                    intent.putExtra("isAddSchedule", UtMapHelper.getMapObjectBoolean(args, "isAddSchedule"));
                }
                break;
            case JumpConstants.EDIFY_ADD_LIST:
                entity.setCode(JumpConstants.NEW_EDIFY_LIST_CODE);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                break;
            case JumpConstants.NATIVE_SEARCH_CONTENT_HOME_CODE:
                entity.setCode(JumpConstants.NEW_SEARCH_MAIN_HOME);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                break;
            case JumpConstants.SCHEDULE_REPEAT_LIST:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_REPEAT_CODE);
                break;
            case JumpConstants.ALBUM_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_HOME_CODE);
                if (args.containsKey("index")) {
                    intent.putExtra("index", UtMapHelper.getMapObject2Int(args, "index"));
                }
                break;
            case JumpConstants.FAMILY_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_FAMILY);
                entity.setCode(JumpConstants.FAMILY_HOME);
                if (args.containsKey("index")) {
                    intent.putExtra("index", UtMapHelper.getMapObject2Int(args, "index"));
                }
                break;
            case JumpConstants.PAY_RECORD_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_HOME_CODE);
                intent.putExtra("index", 6);
                break;
            case JumpConstants.MINE_CLOUD_STORAGE_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_HOME_CODE);
                intent.putExtra("index", 5);
                break;
            case JumpConstants.MINE_ALBUM_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_HOME_CODE);
                intent.putExtra("index", 0);
                break;
            case JumpConstants.SEARCH_MINE_ALBUM_HOME:
                intent.putExtra("albumId", UtMapHelper.getMapObject2Long(args, "uid"));
                intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                break;
            case JumpConstants.NEW_ALBUM_EDIFY_SCHEDULING_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                intent.putExtra("deviceEdifyId", UtMapHelper.getMapObject2Long(args, "deviceEdifyId"));
                intent.putExtra("algorithm", UtMapHelper.getMapObject2String(args, "algorithm"));//算法
                intent.putExtra("day", UtMapHelper.getMapObject2Int(args, "day"));//当前天
                break;
            case JumpConstants.AUDIO_PLAYER_HOME: {
                intent.setAction(Intent.ACTION_VIEW);
                if (args.containsKey("uids")) {
                    String ids = UtMapHelper.getMapObject2String(args, "uids");
                    int index = UtMapHelper.getMapObject2Int(args, "index");
                    intent.setData(Uri.parse("ntt://app/?page=PlayHomeActivity&ids=" + ids + "&index=" + index));
                } else {
                    String data = UtMapHelper.getMapObject2String(args, "data");
                    intent.setData(Uri.parse("ntt://app/?" + data));
                }
            }
            break;
            case JumpConstants.NATIVE_TRACK_DETAIL:
            case JumpConstants.VIDEO_PLAYER_HOME: {
                intent.setAction(Intent.ACTION_VIEW);
                if (args.containsKey("link")) {
                    String link = UtMapHelper.getMapObject2String(args, "link");
                    String[] videoLinks = link.split(",");
                    List<SMediaEntity> medias = new ArrayList<>();
                    for (String videoLink : videoLinks) {
                        SMediaEntity sMediaEntity = new SMediaEntity();
                        sMediaEntity.setLink(videoLink);
                        medias.add(sMediaEntity);
                    }
                    SPlayEntity sPlayEntity = new SPlayEntity.Builder()
                            .setIndex(0)
                            .setType(SPlayEntity.VIDEO)
                            .setMedias(medias)
                            .build();
                    intent.setData(Uri.parse("ntt://app/?" + GsonUtils.toJson(sPlayEntity)));
                } else if (args.containsKey("uids")) {
                    String link = UtMapHelper.getMapObject2String(args, "uids");
                    String[] videoIds = link.split(",");
                    List<SMediaEntity> medias = new ArrayList<>();
                    for (String id : videoIds) {
                        SMediaEntity sMediaEntity = new SMediaEntity();
                        sMediaEntity.setUid(Long.parseLong(id));
                        medias.add(sMediaEntity);
                    }
                    SPlayEntity sPlayEntity = new SPlayEntity.Builder()
                            .setIndex(0)
                            .setType(SPlayEntity.VIDEO)
                            .setMedias(medias)
                            .build();
                    intent.setData(Uri.parse("ntt://app/?" + GsonUtils.toJson(sPlayEntity)));
                } else if (args.containsKey("uid")) {

                    //跳转音频播放
                    List<SMediaEntity> medias = new ArrayList<>();
                    SMediaEntity mediaEntity = new SMediaEntity();
                    mediaEntity.setUid(UtMapHelper.getMapObject2Long(args, "uid"));
                    mediaEntity.setName(UtMapHelper.getMapObject2String(args, "title"));
                    medias.add(mediaEntity);

                    SConfigEntity configEntity = new SConfigEntity();
                    SPlayEntity sPlayEntity = new SPlayEntity.Builder()
                            .setMedias(medias)
                            .setType(SPlayEntity.AUDIO) //播放类型（音视频）
                            .setIndex(0)  //列表位置
                            .setConfig(configEntity)
                            .build();
                    sPlayEntity.toJumpPlayer(mContext);
                } else {
                    String data = UtMapHelper.getMapObject2String(args, "data");
                    intent.setData(Uri.parse("ntt://app/?" + data));
                }
            }
            break;
            case JumpConstants.YOUDAO_FINGER_DETECT:
                entity.setCode(JumpConstants.YOUDAO_FINGER_DETECT_HOME);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_CORESERVICE);
                if (args.containsKey("requestType")) {
                    String requestType = args.get("requestType").toString();
                    Bundle bundle = new Bundle();
                    bundle.putString("requestType", requestType);
                    intent.putExtras(bundle);
                }
                break;
            case JumpConstants.NEW_SYNCHROLOGY_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                if (args.containsKey("gradeId")) {
                    // 年级id
                    intent.putExtra("gradeId", UtMapHelper.getMapObject2Int(args, "gradeId"));
                }
                break;
            case JumpConstants.MEMORY_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_MEMORY);
                entity.setCode(JumpConstants.MEMORY_HOME);
                //couse:课程，mycourse:我的课程，exam:考级，coursedetail: 课程详情 ，"pratical",不传就主界面了
                if (args.containsKey("page")) {
                    intent.putExtra("page", UtMapHelper.getMapObject2String(args, "page"));
                }
                if (args.containsKey("data")) {
                    intent.putExtra("data", args.get("data").toString());
                }
                break;
            case JumpConstants.SCHEDULE_EXECUTE_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                entity.setCode(JumpConstants.SCHEDULE_EDIFY_OR_PUNCH);
                if (args.containsKey("data")) {
                    intent.putExtra("data", args.get("data").toString());
                }
                break;
            case JumpConstants.CARTOON_BOOK_DETAIL_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                entity.setCode(JumpConstants.READING_LIBRARY_CONTENT);
                if (args.containsKey("uid")) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("uid", UtMapHelper.getMapObject2Int(args, "uid"));
                    intent.putExtra("params", bundle);
                }
                break;
            case JumpConstants.BOOK_LIBRARY_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                entity.setCode(JumpConstants.READING_LIBRARY);
                break;
            case JumpConstants.BOOK_SERIES_SETS_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                entity.setCode(JumpConstants.REREAD_LIBRARY_DETAIL);
                Bundle seriesBundle = new Bundle();
                seriesBundle.putInt("uid", UtMapHelper.getMapObject2Int(args, "uid"));
                seriesBundle.putString("type", "BOOK_LIST_GROUP");
                intent.putExtra("params", seriesBundle);
                break;
            case JumpConstants.BOOK_SERIES_REAL:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                entity.setCode(JumpConstants.REREAD_LIBRARY_DETAIL);
                Bundle realBundle = new Bundle();
                realBundle.putInt("uid", UtMapHelper.getMapObject2Int(args, "uid"));
                realBundle.putString("type", "BOOK_LIST");
                intent.putExtra("params", realBundle);
                break;
            case JumpConstants.READING_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                entity.setCode(JumpConstants.READING_READING);
                break;
            case JumpConstants.READING_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                break;
            case JumpConstants.BOOK_SEARCH_CODE:
                entity.setCode(JumpConstants.REREAD_SEARCH);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                break;
            case JumpConstants.BOOK_RACK_HOME_CODE:
                entity.setCode(JumpConstants.READING_LIBRARY);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                intent.putExtra("index", 3);
                break;
            case JumpConstants.REREAD_HISTORY_CODE:
                entity.setCode(JumpConstants.READING_LIBRARY);
                entity.setPackageName(JumpConstants.PACKAGE_NAME_READING);
                intent.putExtra("index", 4);
                break;
            case JumpConstants.SYNCHROLOGY_ADD_TEXTBOOK:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                intent.putExtra("isAdd", true);
                if (args.containsKey("code")) {
                    // 科目code
                    //  * "cn_textbook"; //中文教材
                    //  * "en_textbook"; //英文教材
                    //  * "math_textbook"; //数学教材
                    //  * "science_textbook"; //科学教材
                    intent.putExtra("code", UtMapHelper.getMapObject2String(args, "code"));
                }
                //                if (args.containsKey("gradeId")) {
                //                    // 年级id
                //                    intent.putExtra("gradeId", UtMapHelper.getMapObject2Int(args, "gradeId"));
                //                }
                break;
            case JumpConstants.SYNCHROLOGY_TEXTBOOK_CATALOGUE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                // 是否是玩瞳识别的id
                intent.putExtra("isTextBook", UtMapHelper.getMapObjectBoolean(args, "isTextBook"));
                // 教材 id
                intent.putExtra("bookId", UtMapHelper.getMapObject2Long(args, "bookId"));
                // 教材名  可不传 不传时显示默认文本
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.SYNCHROLOGY_TITLE_KNOWLEDGE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                // 教材标题 id
                intent.putExtra("titleId", UtMapHelper.getMapObject2Long(args, "titleId"));
                // 教材标题
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                // "page": "explain","dictate","recite","read","exercise"
                intent.putExtra("code", UtMapHelper.getMapObject2String(args, "code"));

                if (UtMapHelper.getMapObjectBoolean(args, "isRecognition")) {
                    // 当是玩瞳识别出的结果时传入以下两个参数
                    intent.putExtra("isTextBook", true);
                    // 第几页
                    intent.putExtra("pageIndex", UtMapHelper.getMapObject2Long(args, "pageIndex"));
                }
                break;
            case JumpConstants.SYNCHROLOGY_DETAILS_OF_KNOWLEDGE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                // 知识点id
                intent.putExtra("uid", UtMapHelper.getMapObject2Long(args, "uid"));
                break;
            case JumpConstants.SYNCHROLOGY_DICTATION:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                // 教材标题 id
                intent.putExtra("titleId", UtMapHelper.getMapObject2Long(args, "titleId"));
                break;
            case JumpConstants.SYNCHROLOGY_READ_AND_RECITE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SYNC_LEARNING);
                // type : read , recite
                intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                if (args.containsKey("titleId")) {
                    // 教材标题 id
                    intent.putExtra("titleId", UtMapHelper.getMapObject2Long(args, "titleId"));
                }
                if (args.containsKey("data")) {
                    intent.putExtra("data", UtMapHelper.getMapObject2String(args, "data"));
                }
                break;
            case JumpConstants.EYE_PROTECTION_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_EYE_PROTECTION);
                entity.setCode(JumpConstants.EYE_PROTECTION_HOME);
                break;
            case JumpConstants.CLOUD_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_CLOUD);
                entity.setCode(JumpConstants.CLOUD_HOME);
                if (args.containsKey("type")) {
                    intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                }
                break;
            case JumpConstants.SCHEDULE_CATEGORY:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                entity.setCode(JumpConstants.SCHEDULE_CATEGORY);
                break;
            case JumpConstants.SCHEDULE_MANAGEMENT_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                entity.setCode(JumpConstants.SCHEDULE_HOME);
                break;
            case JumpConstants.WORD_CARD_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_WORD_CARD);
                entity.setCode(JumpConstants.WORD_CARD_HOME);
                break;
            case JumpConstants.WORD_SERIES_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_WORD_CARD);
                entity.setCode(JumpConstants.WORD_SERIES_HOME);
                intent.putExtra("uid", UtMapHelper.getMapObject2Int(args, "uid"));
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.WORD_WORD_DETAILS:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_WORD_CARD);
                entity.setCode(JumpConstants.WORD_WORD_DETAILS);
                // 卡片id
                intent.putExtra("uid", UtMapHelper.getMapObject2Int(args, "uid"));
                // 卡片名
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.SETTING_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SETTING);
                entity.setCode(JumpConstants.SETTING_HOME);
                break;
            case JumpConstants.GROUP_LIST_CODE:
            case JumpConstants.GROUP_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.PUNCH_HOME);
                if (args.containsKey("index")) {
                    intent.putExtra("index", UtMapHelper.getMapObject2Int(args, "index"));
                }
                if (args.containsKey("isScheduleIn")) {
                    intent.putExtra("isScheduleIn", UtMapHelper.getMapObjectBoolean(args, "isScheduleIn"));
                }
                break;
            case JumpConstants.SCHEDULE_ADD_EDIFY_COURSE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                intent.putExtra("courseId", UtMapHelper.getMapObject2Long(args, "courseId"));
                break;
            case JumpConstants.SCHEDULE_ADD_EDIFY_ALBUM:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                if (args.containsKey("edifyId")) {
                    intent.putExtra("edifyId", UtMapHelper.getMapObject2Long(args, "edifyId"));
                }
                if (args.containsKey("albumId")) {
                    intent.putExtra("albumId", UtMapHelper.getMapObject2Long(args, "albumId"));
                }
                break;
            case JumpConstants.SCHEDULE_ADD_SCHEDULE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_SCHEDULE);
                Bundle bundle = new Bundle();
                if (args.containsKey("scheduleId")) {
                    bundle.putLong("scheduleId", UtMapHelper.getMapObject2Long(args, "scheduleId"));
                }
                if (args.containsKey("type")) {
                    bundle.putString("type", UtMapHelper.getMapObject2String(args, "type"));
                }
                if (args.containsKey("uid")) {
                    bundle.putLong("uid", UtMapHelper.getMapObject2Long(args, "uid"));
                }
                if (args.containsKey("bookSetSeriesId")) {
                    bundle.putLong("bookSetSeriesId", UtMapHelper.getMapObject2Long(args, "bookSetSeriesId"));
                }
                if (args.containsKey("bookSeriesId")) {
                    bundle.putLong("bookSeriesId", UtMapHelper.getMapObject2Long(args, "bookSeriesId"));
                }
                if (args.containsKey("bookListType")) {
                    bundle.putString("bookListType", UtMapHelper.getMapObject2String(args, "bookListType"));
                }
                if (args.containsKey("name")) {
                    bundle.putString("name", UtMapHelper.getMapObject2String(args, "name"));
                }
                if (args.containsKey("cateId")) {
                    bundle.putLong("cateId", UtMapHelper.getMapObject2Long(args, "cateId"));
                }
                intent.putExtra("scheduleBundle", bundle);
                break;
            case JumpConstants.NICE_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_NICE);
                entity.setCode(JumpConstants.NICE_HOME);
                if (args.containsKey("themeId")) {
                    intent.putExtra("themeId", UtMapHelper.getMapObject2Int(args, "themeId"));
                }
                if (args.containsKey("themeName")) {
                    intent.putExtra("themeName", UtMapHelper.getMapObject2String(args, "themeName"));
                }
                if (args.containsKey("page")){
                    intent.putExtra("page", UtMapHelper.getMapObject2String(args, "page"));
                }
                if (args.containsKey("unitId")){
                    intent.putExtra("unitId", UtMapHelper.getMapObject2Int(args, "unitId"));
                }
                if (args.containsKey("periodId")){
                    intent.putExtra("periodId", UtMapHelper.getMapObject2Int(args, "periodId"));
                }
                if (args.containsKey("unitName")){
                    intent.putExtra("unitName", UtMapHelper.getMapObject2String(args, "unitName"));
                }
                break;
            case JumpConstants.OPEN_ANY_APPLICATION_CODE:
                if (!TextUtils.isEmpty(entity.getPackageName())) {
                    if (entity.getPackageName().contains("://")) {
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(entity.getPackageName()));
                    } else {
                        entity.setPackageName(entity.getPackageName());
                        entity.setCode(getHomePageByPackageName(entity.getPackageName()));
                    }
                }
                break;
            case JumpConstants.DYNAMIC_CARD_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_LAUNCHER);
                entity.setCode(JumpConstants.VISUAL_CARD_HOME);
                intent.putExtra("pageCode", UtMapHelper.getMapObject2String(args, "pageCode"));
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.DOWNLOAD_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_DOWNLOAD);
                entity.setCode(JumpConstants.DOWNLOAD_NEW_HOME);
                break;
            case JumpConstants.WANG_KE_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_WANG_KE);
                entity.setCode(JumpConstants.WANG_KE_HOME);
                intent.putExtra("intentFlag", UtMapHelper.getMapObject2String(args, "intentFlag"));
                Bundle bundle1 = new Bundle();
                if (args.containsKey("IntentGrade")) {
                    bundle1.putInt("IntentGrade", UtMapHelper.getMapObject2Int(args, "IntentGrade"));
                }
                if (args.containsKey("IntentSubject")) {
                    bundle1.putString("IntentSubject", UtMapHelper.getMapObject2String(args, "IntentSubject"));
                }
                if (args.containsKey("bookID")) {
                    bundle1.putString("bookID", UtMapHelper.getMapObject2String(args, "bookID"));
                }
                if (args.containsKey("IntentCanChangeGradeOrSubject")) {
                    bundle1.putString("IntentCanChangeGradeOrSubject", UtMapHelper.getMapObject2String(args, "IntentCanChangeGradeOrSubject"));
                }
                intent.putExtra("bundle", bundle1);
                break;
            case JumpConstants.APP_ACTIVE_H5_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_LAUNCHER);
                entity.setCode(JumpConstants.H5_WEB_VIEW_HOME);
                intent.putExtra("linkUrl", UtMapHelper.getMapObject2String(args, "linkUrl"));
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.SUBSCRIBE_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_ALBUM);
                entity.setCode(JumpConstants.NEW_ALBUM_SUBSCRIBE_HOME);
                break;
            case JumpConstants.MEMORY_COURSE_INFO_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_MEMORY);
                entity.setCode(JumpConstants.MEMORY_HOME);
                intent.putExtra("page", "coursedetail");
                Map<String, Object> memory = new HashMap<>();
                memory.put("courseId", UtMapHelper.getMapObject2Int(args, "uid"));
                intent.putExtra("data", GsonUtils.toJson(memory));
                break;
            case JumpConstants.RAZ_BOOK_LEARNING:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_RAZ);
                //                intent.putExtra("uid", UtMapHelper.getMapObject2Long(args, "uid"));
                break;
            case JumpConstants.RAZ_HOME_ADD_BOOK:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_RAZ);
                intent.putExtra("isAddBook", UtMapHelper.getMapObjectBoolean(args, "isAddBook"));
                break;
                case JumpConstants.CORRECT_CAMERA:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_CORRECT);
                intent.putExtra("type", UtMapHelper.getMapObject2String(args, "type"));
                break;
            case JumpConstants.GROUP_CARD_MORE_CODE:
            case JumpConstants.READ_BOOK_LIST_CODE:
            case JumpConstants.ALBUM_LIST_CODE:
            case JumpConstants.EDIFY_ALBUM_LIST_CODE:
            case JumpConstants.BROADCASTER_LIST_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_LAUNCHER);
                entity.setCode(JumpConstants.CARD_MORE_HOME);
                intent.putExtra("uid", UtMapHelper.getMapObject2Long(args, "uid"));
                intent.putExtra("title", UtMapHelper.getMapObject2String(args, "title"));
                break;
            case JumpConstants.STORE_HOME_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_STORE);
                entity.setCode(JumpConstants.STORE_HOME);
                break;
            case JumpConstants.STORE_APP_DETAIL_CODE:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_STORE);
                entity.setCode(JumpConstants.STORE_APP_DETAIL);
                intent.putExtra("appUid", UtMapHelper.getMapObject2Long(args, "uid"));
                break;
            case JumpConstants.BIND_DEVICE_HOME:
                entity.setPackageName(JumpConstants.PACKAGE_NAME_LAUNCHER);
                entity.setCode(JumpConstants.BIND_DEVICE_HOME);
                intent.putExtra("unbind", true);
                break;
            default:
                break;
        }
        if (!(mContext instanceof Activity)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        return intent;
    }

    /**
     * 隐式启动
     *
     * @param intent
     * @return
     */
    private boolean isImplicitStartActivity(Intent intent) {
        return !TextUtils.isEmpty(intent.getAction());
    }


    private String getHomePageByPackageName(String packageName) {
        try {
            PackageManager packageManager = mContext.getApplicationContext().getPackageManager();
            Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> apps = packageManager.queryIntentActivities(mainIntent, 0);
            for (ResolveInfo resolveInfo : apps) {
                if (packageName.equals(resolveInfo.activityInfo.packageName)) {
                    return resolveInfo.activityInfo.name;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
