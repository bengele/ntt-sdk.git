/*
 * @Author: tangbing
 *
 *     Copyright (C), 2015 - 2030, ShenZhen Benew Technology Co.,Ltd.
 *
 * @Date: 2022/3/4 上午11:41
 */

package com.ntt.core.service.entities.player;


import android.content.Context;

import androidx.annotation.IntDef;

import com.blankj.utilcode.util.LogUtils;
import com.ntt.core.service.entities.SJumpEntity;
import com.ntt.core.service.supports.jumper.JumpConstants;
import com.ntt.core.service.supports.jumper.JumperManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SPlayEntity {

    public static final int AUDIO = 0;
    public static final int VIDEO = 1;

    @IntDef({AUDIO, VIDEO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {

    }

    private final int type;
    private final List<SMediaEntity> medias;
    private final int index;
    private final SConfigEntity config;
    private final long albumId;

    private SPlayEntity(Builder builder) {
        this.medias = builder.medias;
        this.index = builder.index;
        this.config = builder.config;
        this.albumId = builder.albumId;
        this.type = builder.type;
    }

    @Type
    public int getType() {
        return type;
    }

    public List<SMediaEntity> getMedias() {
        return medias;
    }

    public int getIndex() {
        return index;
    }

    public SConfigEntity getConfig() {
        return config;
    }

    public long getAlbumId() {
        return albumId;
    }

    public static final class Builder {
        private List<SMediaEntity> medias;
        private int index;
        private SConfigEntity config;
        private long albumId;
        private @Type int type;

        /**
         * 列表
         *
         * @param medias
         * @return
         */
        public Builder setMedias(List<SMediaEntity> medias) {
            this.medias = medias;
            return this;
        }

        /**
         * 列表索引
         *
         * @param index
         * @return
         */
        public Builder setIndex(int index) {
            this.index = index;
            return this;
        }

        /**
         * 配置信息
         *
         * @param config
         * @return
         */
        public Builder setConfig(SConfigEntity config) {
            this.config = config;
            return this;
        }

        /**
         * 专辑ID
         *
         * @param albumId
         * @return
         */
        public Builder setAlbumId(long albumId) {
            this.albumId = albumId;
            return this;
        }

        /**
         * 播放类型（音频/视频）
         *
         * @param type
         * @return
         */
        public Builder setType(@Type int type) {
            this.type = type;
            return this;
        }

        public SPlayEntity build() {
            return new SPlayEntity(this);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "\"type\":" + type +
                ",\"medias\":" + medias +
                ",\"index\":" + index +
                ",\"config\":" + config +
                ",\"albumId\":" + albumId +
                '}';
    }

    public void toJumpPlayer(Context context) {
        LogUtils.json(this);
        SJumpEntity sJumpEntity = new SJumpEntity();
        Map<String, Object> params = new HashMap<>();
        params.put("data", toString());
        sJumpEntity.setCode(this.type == AUDIO ? JumpConstants.AUDIO_PLAYER_HOME : JumpConstants.VIDEO_PLAYER_HOME);
        sJumpEntity.setParams(params);
        new JumperManager(context).doJump(sJumpEntity);
    }


}
