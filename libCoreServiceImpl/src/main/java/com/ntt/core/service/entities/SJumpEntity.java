package com.ntt.core.service.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

public class SJumpEntity implements Parcelable {
    private String packageName;
    private String code;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public static Creator<SJumpEntity> getCREATOR() {
        return CREATOR;
    }

    private Map<String, Object> params;


    public void setCode(String v) {
        this.code = v;
    }

    public String getCode() {
        return this.code;
    }

    public void setParams(Map<String, Object> v) {
        this.params = v;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(packageName);
        parcel.writeString(code);
        parcel.writeMap(params);
    }

    public static final Creator<SJumpEntity> CREATOR = new Creator<SJumpEntity>() {
        @Override
        public SJumpEntity createFromParcel(Parcel in) {
            SJumpEntity jumpModel = new SJumpEntity();
            jumpModel.packageName = in.readString();
            jumpModel.code = in.readString();
            jumpModel.params = in.readHashMap(Map.class.getClassLoader());
            return jumpModel;
        }

        @Override
        public SJumpEntity[] newArray(int size) {
            return new SJumpEntity[size];
        }
    };
}
