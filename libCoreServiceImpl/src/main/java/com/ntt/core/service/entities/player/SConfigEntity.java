/*
 * @Author: tangbing
 *
 *     Copyright (C), 2015 - 2030, ShenZhen Benew Technology Co.,Ltd.
 *
 * @Date: 2022/3/4 上午11:44
 */

package com.ntt.core.service.entities.player;

import androidx.annotation.IntDef;

/**
 * 可扩展配置参数
 */
public class SConfigEntity {

    /**
     * 临时列表
     */
    public static final int TEMP = 0;
    /**
     * 云盘列表
     */
    public static final int CLOUD = 1;
    /**
     * 打卡
     */
    public static final int PUNCH_CARD = 2;
    /**
     * 熏陶
     */
    public static final int EDIFY = 3;

    @IntDef({TEMP, CLOUD, PUNCH_CARD, EDIFY})
    public @interface Category {

    }

    /**
     * 类型
     */
    private int category;

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
