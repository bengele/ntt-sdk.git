/*
 * @author: tangbing
 *
 *         Copyright (C), 2019 - 2030, ShenZhen Benew Technology Co.,Ltd.
 *
 * @date: 19-3-21 上午10:26
 */

package com.ntt.core.service.supports.jumper;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;

public class UtMapHelper {


    public static Double getMapObject2Double(Map<String, Object> map, String key) {
        if (map == null) {
            return 0.00;
        }

        if (!map.containsKey(key)) {
            return 0.00;
        }
        if (map.get(key) == null) {
            return 0.00;
        }

        if (map.get(key) instanceof Double) {
            return (Double) map.get("key");
        } else {
            return 0.00;
        }
    }

    /**
     * 获取map中boolean类型的value
     *
     * @param map
     * @param key
     * @return
     */
    public static boolean getMapObjectBoolean(Map<String, Object> map, String key) {
        if (map == null) {
            return false;
        }
        if (!map.containsKey(key)) {
            return false;
        }
        if (map.get(key) == null) {
            return false;
        }
        if (map.get(key) instanceof Boolean) {
            return (boolean) map.get(key);
        } else {
            return false;
        }
    }

    /**
     * @param map
     * @param key
     * @return
     */
    public static String getMapObject2String(Map<String, Object> map, String key) {
        if (map == null) {
            return "";
        }
        if (!map.containsKey(key)) {
            return "";
        }
        if (map.get(key) == null) {
            return "";
        }
        if (map.get(key) instanceof Integer) {
            return map.get(key) + "";
        }
        if (map.get(key) instanceof Double) {
            return map.get(key) + "";
        }
        if (map.get(key) instanceof Float) {
            return map.get(key) + "";
        }
        if (map.get(key) instanceof String) {
            return (String) map.get(key);
        } else {
            return "";
        }
    }

    /**
     * 获取map中int类型的value
     *
     * @param map
     * @param key
     * @return
     */
    public static int getMapObject2Int(Map<String, Object> map, String key) {
        if (map == null) {
            return 0;
        }

        if (!map.containsKey(key)) {
            return 0;
        }

        if (map.get(key) == null) {
            return 0;
        }
        try {
            if (map.get(key) instanceof Integer) {
                return (int) map.get(key);
            } else if (map.get(key) instanceof Double) {
                return Double.valueOf(Objects.requireNonNull(map.get(key)).toString()).intValue();
            } else if (map.get(key) instanceof BigDecimal) {
                return Double.valueOf(Objects.requireNonNull(map.get(key)).toString()).intValue();
            } else if (map.get(key) instanceof String) {
                return Integer.parseInt(Objects.requireNonNull(map.get(key)).toString());
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取map中long类型的value
     *
     * @param map
     * @param key
     * @return
     */
    public static long getMapObject2Long(Map<String, Object> map, String key) {
        if (map == null) {
            return 0;
        }
        if (!map.containsKey(key)) {
            return 0;
        }
        if (map.get(key) == null) {
            return 0;
        }
        if (map.get(key) instanceof Long) {
            return (long) map.get(key);
        } else if (map.get(key) instanceof Integer) {
            return (((Integer) Objects.requireNonNull(map.get(key))).longValue());
        } else if (map.get(key) instanceof Double) {
            return Double.valueOf(Objects.requireNonNull(map.get(key)).toString()).longValue();
        } else if (map.get(key) instanceof String) {
            return Integer.parseInt(Objects.requireNonNull(map.get(key)).toString());
        } else {
            return 0;
        }
    }


}
