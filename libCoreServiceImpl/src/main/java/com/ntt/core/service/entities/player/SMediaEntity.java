/*
 * @Author: tangbing
 *
 *     Copyright (C), 2015 - 2030, ShenZhen Benew Technology Co.,Ltd.
 *
 * @Date: 2022/3/4 上午11:42
 */

package com.ntt.core.service.entities.player;

public class SMediaEntity {

    private long uid;
    private String name;
    private String link;
    private String userAgent;


    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Override
    public String toString() {
        return "{" +
                "\"uid\":" + uid +
                ",\"name\":\"" + name + ('\"') +
                ",\"link\":\"" + link + ('\"') +
                ",\"userAgent\":\"" + userAgent + ('\"') +
                '}';
    }
}
