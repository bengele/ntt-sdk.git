package com.ntt.core.service.supports.jumper;


public class JumpConstants {

    public static final String NATIVE_ALBUM_DETAIL_CODE = "AlbumDetail";                            //专辑详情
    public static final String NATIVE_SEARCH_CONTENT_HOME_CODE = "SearchContentHome";               //全局搜索
    public static final String NATIVE_BROADCASTER_DETAIL_CODE = "BroadcasterDetail";                //主播详情
    public static final String NATIVE_EDIFY_PLAN_DETAIL_CODE = "EdifyPlanDetail";                   //熏陶课程详情
    public static final String NATIVE_ALBUM_CAT_HOME = "AlbumCatHome";                              //首页分类
    public static final String NATIVE_ANNOUNCER_HOME = "AnnouncerHome";                             //金牌主播
    public static final String NATIVE_EDIFY_HOME = "EdifyHome";                                     //熏陶广场
    public static final String NATIVE_EDIFY_ALBUM_DETAIL = "EdifyAlbumDetail";                      //熏陶专辑详情
    public static final String NATIVE_TRACK_DETAIL = "TrackDetail";                                 //搜索音频跳转
    public static final String SCHEDULE_REPEAT_LIST = "ScheduleRepeatClassify";                     //日程复读类型列表
    public static final String YOUDAO_FINGER_DETECT = "YouDaoFingerDetect";                         //有道指尖查词
    public static final String SCHEDULE_EXECUTE_CODE = "ScheduleExecute";                           //日程管理熏陶或者打卡
    public static final String CARTOON_BOOK_DETAIL_CODE = "CartoonbookDetail";                      //绘本详情
    public static final String BOOK_LIBRARY_CODE = "BookLibrary";                                   //绘本馆
    public static final String READING_CODE = "Reading";                                            //读书
    public static final String NATIVE_ALBUM_GROUP_COURSE_DETAIL = "GroupCourseDetail";              //打卡详情页                    //打卡详情页
//    public static final String CLOUD_HOME_CODE = "CloudHome";                                       //百度网盘
    public static final String SCHEDULE_MANAGEMENT_CODE = "ScheduleManagement";                     //日程管理
    public static final String GROUP_HOME_CODE = "GroupHome";                                       //打卡主页
    public static final String ALBUM_HOME_CODE = "AlbumHome";                                       //熏陶主页
    public static final String PAY_RECORD_CODE = "PayRecord";                                       //我的购买
    public static final String MINE_CLOUD_STORAGE_HOME_CODE = "MineCloudStorageHome";                    //我的云盘
    public static final String MINE_ALBUM_HOME_CODE = "MineAlbumHome";                                   //我的收藏
    public static final String OPEN_ANY_APPLICATION_CODE = "OpenAnyApplication";                    //打开任何应用
    public static final String DYNAMIC_CARD_HOME_CODE = "DynamicCardHome";                          //可视化卡片
    public static final String EDIFY_ADD_LIST = "EdifyAddList";                                     //熏陶专辑
    public static final String FAMILY_HOME_CODE = "AccountFamilyMemberList";                                     //家庭
    public static final String BOOK_SEARCH_CODE = "BookSearch";                                          //绘本搜索
    public static final String BOOK_RACK_HOME_CODE = "BookrackHome";                                     //我的书架
    public static final String REREAD_HISTORY_CODE = "RereadHistory";                                    //读书历史记录
    public static final String EDIFY_COURSE_DETAIL_CODE = "EdifyCourseDetail";                      //熏陶课程
    public static final String BOOK_SERIES_SETS_CODE = "BookSeriesSets";                                 //绘本套系
    public static final String BOOK_SERIES_REAL = "BookSeriesReal";                                //通用的绘本系类
    public static final String ALBUM_COMBINE_DETAIL ="AlbumCombineDetail";                          //专辑合集详情
    public static final String APP_ACTIVE_H5_HOME_CODE = "AppActiveH5Home";                              //H5网页
    public static final String GROUP_LIST_CODE = "GroupList";                                       //打卡列表
    public static final String SUBSCRIBE_HOME_CODE = "SubscribeHome";                               //订阅主页
    public static final String MEMORY_COURSE_INFO_CODE = "MemoryCourseInfo";                        //超记课程
    public static final String GROUP_CARD_MORE_CODE = "GroupCardMore";                              //打卡查看更多
    public static final String READ_BOOK_LIST_CODE = "ReadBookList";                                //绘本查看更多
    public static final String ALBUM_LIST_CODE = "AlbumList";                                       //专辑列表
    public static final String TRACK_LIST_CODE = "TrackList";                                       //音频列表
    public static final String EDIFY_ALBUM_LIST_CODE = "EdifyAlbumList";                            //推荐熏陶列表
    public static final String BROADCASTER_LIST_CODE = "BroadcasterList";                           //主播列表
    public static final String STORE_HOME_CODE = "StoreHome";                                       //应用市场主页
    public static final String STORE_APP_DETAIL_CODE = "StoreAppDetail";                                 //应用市场详情页
    public static final String NATIVE_ALBUM_VIDEO_DETAIL_CODE = "AlbumVideoDetail";                 //视频专辑详情
    public static final String NATIVE_GUIDE_VIDEO_CODE = "VideoGuild";                        //引导视频

    /***** Package name ****/
    public static final String PACKAGE_NAME_LAUNCHER = "com.benew.nttr.launcher";
    public static final String PACKAGE_NAME_ALBUM = "com.benew.nttr.r2album";
    public static final String PACKAGE_NAME_PLAYER = "com.benew.nttr.player";
    public static final String PACKAGE_NAME_CORESERVICE = "com.ntt.core.service";
    public static final String PACKAGE_NAME_SCHEDULE = "com.benew.nttr.schedule"; //日程管理
    public static final String PACKAGE_NAME_MEMORY = "com.benew.nttr2.memory";
    public static final String PACKAGE_NAME_READING = "com.benew.ntt.reading";  //读书
    public static final String PACKAGE_NAME_SYNC_LEARNING = "com.benew.nttr2.synclearning"; //同步学
    public static final String PACKAGE_NAME_EYE_PROTECTION = "com.benew.nttr2.eyeprotection"; //护眼模式
    public static final String PACKAGE_NAME_CLOUD = "com.benew.nttr.cloud"; //百度云
    public static final String PACKAGE_NAME_WORD_CARD = "com.benew.wordcard";    // 中英字卡包名
    public static final String PACKAGE_NAME_SETTING = "com.benew.nttr2.settings"; //设置
    public static final String PACKAGE_NAME_NICE = "com.benew.nttr2.nice";   //Nice
    public static final String PACKAGE_NAME_DOWNLOAD = "com.benew.ntt.myutils";     //下载
    public static final String PACKAGE_NAME_FAMILY = "com.ntt.family";     //家庭
    public static final String PACKAGE_NAME_WANG_KE = "com.wyt.wangkexueximvvm";
    public static final String PACKAGE_NAME_RAZ = "com.benew.nttr.raz";
    public static final String PACKAGE_NAME_STORE = "com.benew.nttr.store";
    public static final String PACKAGE_NAME_VIDEO = "com.benew.nttr2.video";//视频专辑
    public static final String PACKAGE_NAME_GUIDE = "com.benew.nttr2.guide";//视频引导
    public static final String PACKAGE_NAME_CORRECT = "com.benew.nttr.correct";//批改

    /**** Album Code***/
    public static final String SEARCH_MINE_ALBUM_HOME = "com.benew.nttr.search.ui.SearchMineAlbumActivity"; //搜索自己的专辑
    public static final String NEW_ALBUM_DETAIL_CODE = "com.benew.nttr.album.ui.activity.AlbumDetailActivity";
    public static final String NEW_ALBUM_SERIES_CODE = "com.benew.nttr.album.ui.activity.SeriesActivity";//专辑合集详情页
    public static final String NEW_ALBUM_PUNCH_DETAIL_CODE = "com.benew.nttr.punch.ui.activity.PunchDetailActivity";
    public static final String NEW_EDIFY_PACKAGE_DETAIL_CODE = "com.benew.nttr.edify.ui.activity.EdifyPackageDetailActivity";
    public static final String NEW_EDIFY_ALBUM_DETAIL_CODE = "com.benew.nttr.edify.ui.activity.EdifyAlbumDetailActivity";
    public static final String NEW_ANNOUNCER_DETAIL_CODE = "com.benew.nttr.announcer.ui.activity.AnnouncerDetailActivity";
    public static final String NEW_ANNOUNCER_HOME_CODE = "com.benew.nttr.announcer.ui.activity.AnnouncerMainActivity";
    public static final String NEW_ALBUM_CATEGORY_CODE = "com.benew.nttr.album.ui.activity.AlbumCategoryActivity";
    public static final String NEW_EDIFY_LIST_CODE = "com.benew.nttr.edify.ui.activity.EdifyListActivity";
    public static final String NEW_SEARCH_MAIN_HOME = "com.benew.nttr.search.ui.SearchMainActivity";
    public static final String NEW_ALBUM_REPEAT_CODE = "com.benew.nttr.album.ui.activity.AlbumRepeatActivity";
    public static final String NEW_ALBUM_HOME_CODE = "com.benew.nttr.r2album.ui.MainActivity";
    public static final String NEW_ALBUM_EDIFY_SCHEDULING_CODE="com.benew.nttr.edify.ui.activity.EdifySchedulingActivity";//熏陶专辑排期
    public static final String NEW_ALBUM_SUBSCRIBE_HOME = "com.benew.nttr.album.ui.activity.SubscribeActivity"; //订阅主页
    public static final String NEW_ALBUM_DOWNLOAD_TRACK = "com.benew.nttr.album.ui.activity.AlbumDownloadTrackActivity";//本地下载的专辑

    /*** AlbumVideo Code***/
    public static final String NEW_ALBUM_VIDEO_DETAIL = "com.benew.nttr.r2albumVideo.ui.AlbumVideoDetailActivity";

    /*** VideoGuild Code***/
    public static final String NEW_Guild_VIDEO = "com.benew.r2_guide.ui.MainActivity";

    /**Family**/
    public static final String FAMILY_HOME = "com.ntt.family.mvp.activities.FamilyHomeActivity";

    /*****Player Code *****/
    public static final String AUDIO_PLAYER_HOME = "com.benew.nttr.player.mvp.ui.activity.PlayerHomeActivity";
    public static final String VIDEO_PLAYER_HOME = "com.benew.nttr.player.mvp.ui.activity.VideoHomeActivity";

    /*****应用市场*/
    public static final String STORE_HOME = "com.benew.nttr.store.ui.presenter.home.StoreHomeActivity";
    public static final String STORE_APP_DETAIL = "com.benew.nttr.store.ui.presenter.app.StoreAppDetailActivityPresenter";

    /**
     * 设备绑定
     */
    public static final String BIND_DEVICE_HOME = "com.benew.nttr.launcher.mvp.ui.activity.RegisterQRCodeActivity";
    /**
     * 可视化卡片
     */
    public static final String VISUAL_CARD_HOME = "com.benew.nttr.launcher.mvp.ui.activity.VisualCardActivity";
    /**
     * H5页面
     */
    public static final String H5_WEB_VIEW_HOME = "com.benew.nttr.launcher.mvp.ui.activity.WebViewActivity";

    /**
     * 更多卡片
     */
    public static final String CARD_MORE_HOME = "com.benew.nttr.launcher.mvp.ui.activity.CardMoreActivity";


    /**
     * CoreService Code
     */
    //有道指尖查词
    public static final String YOUDAO_FINGER_DETECT_HOME = "com.ntt.core.service.logic.finger.YouDaoMainActivity";

    /**
     * 同步学 code
     **/
    // 同步学首页
    public static final String NEW_SYNCHROLOGY_HOME = "com.benew.nttr2.synclearning.ui.activity.BookHomeActivity";
    // 同步学添加教材
    public static final String SYNCHROLOGY_ADD_TEXTBOOK = "com.benew.nttr2.synclearning.ui.activity.BookSearchActivity";
    // 教材目录
    public static final String SYNCHROLOGY_TEXTBOOK_CATALOGUE = "com.benew.nttr2.synclearning.ui.activity.BookCatalogActivity";
    // 教材某页详情
    public static final String SYNCHROLOGY_TITLE_KNOWLEDGE = "com.benew.nttr2.synclearning.ui.activity.BookTitleKnowledgeMapActivity";
    // 知识点详情页 && 知识点探索
    public static final String SYNCHROLOGY_DETAILS_OF_KNOWLEDGE = "com.benew.nttr2.synclearning.ui.activity.BookKnowledgeExploreActivity";
    // 听写
    public static final String SYNCHROLOGY_DICTATION = "com.benew.nttr2.synclearning.ui.activity.BookListenWordsActivity";
    // 朗读 && 背诵
    public static final String SYNCHROLOGY_READ_AND_RECITE = "com.benew.nttr2.synclearning.ui.activity.BookReciteWordActivity";

    /**
     * Schedule Code
     */
    //日程管理执行日程
    public static final String SCHEDULE_EDIFY_OR_PUNCH = "com.benew.nttr.schedule.mvp.ui.activity.IntermediaryActivity";
    //添加日程页面
    public static final String SCHEDULE_CATEGORY = "com.benew.nttr.schedule.mvp.ui.activity.CategoryActivity";
    //日程首页
    public static final String SCHEDULE_HOME = "com.benew.nttr.schedule.mvp.ui.activity.MainActivity";
    //添加熏陶课程
    public static final String SCHEDULE_ADD_EDIFY_COURSE = "com.benew.nttr.schedule.mvp.ui.activity.AddEdifyCourseActivity";
    //添加熏陶专辑
    public static final String SCHEDULE_ADD_EDIFY_ALBUM = "com.benew.nttr.schedule.mvp.ui.activity.EdifyAlbumDetailActivity";
    //添加日程
    public static final String SCHEDULE_ADD_SCHEDULE = "com.benew.nttr.schedule.mvp.ui.activity.ScheduleDetailActivity";
    /**
     * Memory Code
     */
    public static final String MEMORY_HOME = "com.example.flutter_code.MainActivity";

    /**
     * Reading
     **/
    public static final String READING_LIBRARY_CONTENT = "com.benew.ntt.reading.mvp.view.LibraryContentActivity";//绘本详情
    public static final String READING_LIBRARY = "com.benew.ntt.reading.mvp.view.LibraryActivity";//绘本馆
    public static final String READING_READING = "com.benew.ntt.reading.mvp.view.VtstoryBaseActivity";//读书
    public static final String READING_HOME = "com.benew.ntt.reading.SplashActivity"; // 读书首页
    public static final String REREAD_SEARCH = "com.benew.ntt.reading.mvp.view.LibrarySearchActivity"; //搜索
    public static final String REREAD_LIBRARY_DETAIL = "com.benew.ntt.reading.mvp.view.LibraryDetailActivity"; //套系/系列

    /**
     * eyeprotection 护眼模式
     */
    public static final String EYE_PROTECTION_HOME = "com.benew.nttr2.eyeprotection.ui.activity.EyeProtectionHomeActivity";

    /**
     * Cloud code
     */
    //百度云
    public static final String CLOUD_HOME = "com.benew.nttr.cloud.mvp.ui.activity.MainActivity";

    /**
     * 中英字卡
     * wordcard code
     */
    // 中英字卡首页
    public static final String WORD_CARD_HOME = "com.benew.wordcard.MainActivity";
    // 套系列表页
    public static final String WORD_SERIES_HOME = "com.benew.wordcard.WordTabHomeActivity";
    // 某个卡的详情页
    public static final String WORD_WORD_DETAILS = "com.benew.wordcard.checkpoint.WordCardDetailsActivity";


    /**
     * 设置
     */
    public static final String SETTING_HOME = "com.benew.nttr2.settings.mvp.ui.activity.SettingHomeActivity"; //首页


    /***********打卡**************/
    //打卡首页
    public static final String PUNCH_HOME = "com.benew.nttr.punch.ui.activity.PunchHomeActivity";
    //打卡详情
    public static final String PUNCH_DETAIL = "com.benew.nttr.punch.ui.activity.PunchDetailActivity";


    /************Nice***************/
    public static final String NICE_HOME = "com.example.niceapp.MainActivity";


    /************下载管理首页**********/
    public static final String DOWNLOAD_HOME = "com.benew.ntt.myutils.ui.DownloadActivity";
    public static final String DOWNLOAD_NEW_HOME = "com.benew.ntt.myutils.v2.activity.DownloadManagerActivity";


    /***************格林****************/
    public static final String WANG_KE_HOME = "com.wyt.wangkexueximvvm.activity.InvokeActivity";

    /************************  RAZ    ***************** */
    // RAZ 绘本学习页
    public static final String RAZ_BOOK_LEARNING = "com.benew.nttr.raz.ui.activity.SchedulerLearningActivity";
    // RAZ 添加绘本页
    public static final String RAZ_HOME_ADD_BOOK = "com.benew.nttr.raz.ui.activity.ReadPlanAddBookActivity";

    /************************  批改    ***************** */
    // 拍照页
    public static final String CORRECT_CAMERA = "com.benew.nttr.correct.mvp.ui.activity.CameraPhotoActivity";



    //设置WI-FI页
    public static final int SETTING_WIFI_CODE = 0;
    //设置蓝牙页
    public static final int SETTING_BLUETOOTH_CODE = 1;
    //设置亮度页
    public static final int SETTING_BRIGHTNESS_CODE = 2;
    //设置声音页
    public static final int SETTING_VOICE_CODE = 3;
}
