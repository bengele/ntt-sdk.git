# ntt-sdk
ntt

### 初始化

```
/**
         * context：上下文
         * appId
         * appSecret
         * debug : 默认为false，使用的是在线服务器的请求接口，true：使用测试服务器的接口。适用无此参数接口
         */
        CoreServiceImpl.init(this, "appId", "appSecret");
```
